package com.lagou.serialization;

import com.alibaba.fastjson.JSON;
import com.lagou.model.RPCMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * <p>Title: 提供者編碼器</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: http://www.ubisys.com.cn/</p>
 *
 * @Auther: cw
 * @Date: 2020/6/18 21:29
 */
public class MyRPCEncoder extends MessageToByteEncoder<RPCMessage> {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, RPCMessage rpcMessage, ByteBuf byteBuf) throws Exception {
        byte[] bytes= JSON.toJSONBytes(rpcMessage);
        byteBuf.writeInt(bytes.length);
        byteBuf.writeBytes(bytes);
    }
}

package com.lagou.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class RPCMessage implements Serializable {
	private static final long serialVersionUID = 7084843947860990140L;

	private String reqId;
	//消息的类型   1 请求；  2  响应
	private  Integer type;

	/**
	 * 请求接口类名
	 */
	private String className;

	/**
	 * 请求方法名
	 */
	private String method;

	/**
	 * 请求参数集合
	 */
	private Map  reqParams;

	/**
	 * 返回结果集
	 */
	private Object respParams;

}

package com.lagou.constants;

/**
 * <p>Title: 常量  </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: http://www.ubisys.com.cn/</p>
 *
 * @Auther: cw
 * @Date: 2020/6/20 19:16
 */
public class Constants {


    /**
     * 请求
     */
    public   static  Integer  REQUEST=1;
    /**
     * 响应
     */
    public  static  Integer  RESPONSE=2;

}

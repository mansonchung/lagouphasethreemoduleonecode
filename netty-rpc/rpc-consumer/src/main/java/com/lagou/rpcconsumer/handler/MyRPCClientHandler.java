package com.lagou.rpcconsumer.handler;

import com.alibaba.fastjson.JSON;
import com.lagou.model.RPCMessage;
import com.lagou.rpcconsumer.controller.ClientController;
import com.lagou.rpcconsumer.util.SpringContextUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;


/**
 * <p>Title: 客户端消息接收處理器</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: http://www.ubisys.com.cn/</p>
 *
 * @Auther: cw
 * @Date: 2020/6/18 22:06
 */
@Slf4j
public class MyRPCClientHandler extends ChannelInboundHandlerAdapter {


    private RedisTemplate redisTemplate;

    public MyRPCClientHandler() {
        SpringContextUtil.getBean(ClientController.class);
        this.redisTemplate = SpringContextUtil.getBean(StringRedisTemplate.class);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg)
            throws Exception {

        System.out.println("client receive msg" + msg);
        RPCMessage rpcMessage = (RPCMessage) msg;
        redisTemplate.opsForValue().set(rpcMessage.getReqId() + "resp", rpcMessage.getRespParams());

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
            throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}

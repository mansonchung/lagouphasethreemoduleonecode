package com.lagou.rpcconsumer.controller;

import com.lagou.pojo.User;
import com.lagou.rpcconsumer.client.NettyRPCClient;
import com.lagou.rpcconsumer.util.SpringContextUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: http://www.ubisys.com.cn/</p>
 *
 * @Auther: cw
 * @Date: 2020/6/20 20:13
 */
@RestController
public class ClientController {


    private RedisTemplate redisTemplate;

    @RequestMapping(value = "/test")
    public Map test(@RequestParam String username, @RequestParam int age, @RequestParam String seq) throws InterruptedException {

        User user = new User();
        user.setAge(age);
        user.setUsername(username);
        NettyRPCClient.sendMessToServer(user, seq);
        Map result = new HashMap();
        result.put("code", 200);
        result.put("result", "success");
        return result;
    }


}

package com.lagou.rpcconsumer.client;

import com.lagou.constants.Constants;
import com.lagou.model.RPCMessage;
import com.lagou.pojo.User;
import com.lagou.rpcconsumer.handler.MyRPCClientHandler;
import com.lagou.serialization.MyRPCDecoder;
import com.lagou.serialization.MyRPCEncoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.HashMap;
import java.util.Map;


//客户端给服务器发送数据
public class NettyRPCClient {

    //1.创建连接池对象
    private static NioEventLoopGroup group = new NioEventLoopGroup();
  // 通道
    private   static Channel channel;


public static void   start(){
    //2.创建客户端的启动引导类 BootStrap
    Bootstrap bootstrap = new Bootstrap();

    //3.配置启动引导类
    bootstrap.group(group)
            //设置通道为Nio
            .channel(NioSocketChannel.class)
            //设置Channel初始化监听
            .handler(new ChannelInitializer<Channel>() {
                //当前该方法监听channel是否初始化
                protected void initChannel(Channel channel) throws Exception {
                    //设置编码
                    channel.pipeline().addLast(new MyRPCEncoder());
                    channel.pipeline().addLast(new MyRPCDecoder());
                    channel.pipeline().addLast(new MyRPCClientHandler());
                }
            });

    //4.使用启动引导类连接服务器 , 获取一个channel
     channel = bootstrap.connect("127.0.0.1", 9999).channel();


}


    /**
     *
     */
    public static void   sendMessToServer(User user, String reqId){


        RPCMessage rpcMessage =new RPCMessage();
        rpcMessage.setReqId(reqId);
        rpcMessage.setType(Constants.REQUEST);
        Map reqMap=new HashMap<>();
        reqMap.put("username",user.getUsername());
        reqMap.put("age",user.getAge());
        rpcMessage.setReqParams(reqMap);
        rpcMessage.setClassName("userservice");
        rpcMessage.setMethod("sayHello");
        //给服务器写数据
        channel.writeAndFlush(rpcMessage);


    }


    public static void main(String[] args) throws InterruptedException {
        start();

        Thread.sleep(2000);
    }

}

package com.lagou.rpcconsumer;

import com.lagou.rpcconsumer.client.NettyRPCClient;
import com.lagou.rpcconsumer.util.SpringContextUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class RpcConsumerApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext =   SpringApplication.run(RpcConsumerApplication.class, args);
        //将run方法的返回值赋值给工具类中的静态变量
        SpringContextUtil.applicationContext = applicationContext;
        //测试获取已经实例化的接口bean，执行bean中方法
        NettyRPCClient.start();
    }

}

package com.lagou.rpcprovider.service;

import ch.qos.logback.classic.Logger;
import com.lagou.pojo.User;
import com.lagou.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>Title:IUserService 实现类 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: http://www.ubisys.com.cn/</p>
 *
 * @Auther: cw
 * @Date: 2020/6/20 19:51
 */
@Service(value = "userservice")
@Slf4j
public class UserServiceImpl implements IUserService {
    @Override
    public String sayHello(User user) {
        System.out.println("  server  speak to  "+ user.getUsername()+"  say ：It's getting dark. Wash up and go to bed !");
        return "success";
    }
}

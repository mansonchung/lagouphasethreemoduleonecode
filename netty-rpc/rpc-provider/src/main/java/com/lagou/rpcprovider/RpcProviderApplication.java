package com.lagou.rpcprovider;

import com.lagou.rpcprovider.server.NettyRPCServer;
import com.lagou.rpcprovider.util.SpringContextUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class RpcProviderApplication {

    public static void main(String[] args) throws InterruptedException {
        ConfigurableApplicationContext applicationContext =   SpringApplication.run(RpcProviderApplication.class, args);
        //将run方法的返回值赋值给工具类中的静态变量
        SpringContextUtil.applicationContext = applicationContext;
        NettyRPCServer.start();
    }

}

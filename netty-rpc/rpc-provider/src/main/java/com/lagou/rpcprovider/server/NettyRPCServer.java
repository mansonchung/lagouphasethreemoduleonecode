package com.lagou.rpcprovider.server;

import com.lagou.rpcprovider.handler.MyRPCServerHandler;
import com.lagou.serialization.MyRPCDecoder;
import com.lagou.serialization.MyRPCEncoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

// 接收客户端请求,打印在控制台
public class NettyRPCServer {


    //bossGroup 负责接收用户连接
    private  static  NioEventLoopGroup bossGroup = new NioEventLoopGroup();

    //workGroup 负责处理用户的io读写操作
    private  static  NioEventLoopGroup workGroup = new NioEventLoopGroup();
    //2.创建启动引导类
private  static   ServerBootstrap serverBootstrap = new ServerBootstrap();




public static  void  start()throws InterruptedException{
    //添加到组中,两个线程池,第一个位置的线程池就负责接收,第二个参数就负责读写
    serverBootstrap.group(bossGroup,workGroup)
            //给我们当前设置一个通道类型
            .channel(NioServerSocketChannel.class)
            //绑定一个初始化监听
            .childHandler(new ChannelInitializer<NioSocketChannel>() {
                //事件监听Channel通道
                protected void initChannel(NioSocketChannel nioSocketChannel) throws Exception {
                    //获取pipeLine
                    ChannelPipeline pipeline = nioSocketChannel.pipeline();
                    //绑定编码
                    pipeline.addFirst(new MyRPCEncoder());
                    pipeline.addLast(new MyRPCDecoder());
                    //绑定我们的业务逻辑
                    pipeline.addLast(new MyRPCServerHandler());
                }
            });

    //4.启动引导类绑定端口
    ChannelFuture future = serverBootstrap.bind(9999).sync();

    //5.关闭通道
  //  future.channel().closeFuture().sync();


}


    public static void main(String[] args) throws InterruptedException {

start();








    }
}

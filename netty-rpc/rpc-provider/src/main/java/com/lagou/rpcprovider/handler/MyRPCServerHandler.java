package com.lagou.rpcprovider.handler;

import com.alibaba.fastjson.JSON;
import com.lagou.constants.Constants;
import com.lagou.model.RPCMessage;
import com.lagou.rpcprovider.util.ReflectionUtil;
import com.lagou.rpcprovider.util.SpringContextUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;

/**
 * <p>Title: 服务端消息接收處理器</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: http://www.ubisys.com.cn/</p>
 *
 * @Auther: cw
 * @Date: 2020/6/18 22:06
 */
@Slf4j
public class MyRPCServerHandler extends ChannelInboundHandlerAdapter {

    private ReflectionUtil reflectionUtil= SpringContextUtil.getBean(ReflectionUtil.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg)
            throws Exception {

        System.out.println("server receive msg: "+msg);

       RPCMessage reqMessage = (RPCMessage) msg;
       //   获取容器中的实例，执行目标方法
        Object result = reflectionUtil.invokeService(reqMessage.getClassName(), reqMessage.getMethod(), reqMessage.getReqParams());
        System.out.println("  方法執行后結果为: "+result);
        RPCMessage respMessage =new RPCMessage();
        BeanUtils.copyProperties(respMessage,reqMessage);
        respMessage.setType(Constants.RESPONSE);
        respMessage.setRespParams(result);
        //返回数据给客户端
        ctx.writeAndFlush(respMessage);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
            throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
